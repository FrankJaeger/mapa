//
//  ViewController.m
//  Mapa
//
//  Created by Przemek on 26.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property bool canUseLocation;

@end

@implementation ViewController

- (IBAction)getUserLocation:(id)sender {
    [ self reloadUserLocation:_mapLocations[@"User"] ];
    [ self showLocation:_mapLocations[@"User"] withZoom:0.01 ];
}

- (IBAction)getDoRLocation:(id)sender {
    [ self showLocation:_mapLocations[@"DroidsOnRoids"] withZoom:0.01 ];
}

- (IBAction)getC13Location:(id)sender {
    [ self showLocation:_mapLocations[@"C13"] withZoom:0.01 ];
}

- (IBAction)getStadiumLocation:(id)sender {
    [ self showLocation:_mapLocations[@"Stadium"] withZoom:0.01 ];
}

- (IBAction)mapTypeChanged:(id)sender {
    
    switch ( [ sender selectedSegmentIndex ] ) {
        case 0:
            [ _theMap setMapType:MKMapTypeStandard ];
            [ self changeLayoutColor: [ [[UIApplication sharedApplication] delegate] window].tintColor ];
            break;
        case 1:
            [ _theMap setMapType:MKMapTypeSatellite ];
            [ self changeLayoutColor:[ UIColor whiteColor ] ];
            break;
        case 2:
            [ _theMap setMapType:MKMapTypeHybrid ];
            [ self changeLayoutColor:[ UIColor whiteColor ] ];
            break;
    }
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _locMan = [ [CLLocationManager alloc] init ];
    _theMap.delegate = self;
    _locMan.delegate = (id)self;
    
    _mapLocations = [[ NSMutableDictionary alloc] init];
    _mapLocations = [ @{
        @"User"          : [ MapLocation getWithLatitude:_locMan.location.coordinate.latitude andLongitude:_locMan.location.coordinate.longitude name:NULL andDescription:NULL ],
        @"DroidsOnRoids" : [ MapLocation getWithLatitude:51.108982  andLongitude:17.02919 name:NSLocalizedString(@"loc_dor_name", NULL) andDescription:NSLocalizedString(@"loc_dor_desc", NULL) ],
        @"C13"           : [ MapLocation getWithLatitude:51.107556 andLongitude:17.059015 name:NSLocalizedString(@"loc_c13_name", NULL) andDescription:NSLocalizedString(@"loc_c13_desc", NULL) ],
        @"Stadium"       : [ MapLocation getWithLatitude:51.141207 andLongitude:16.943772 name:NSLocalizedString(@"loc_sta_name", NULL) andDescription:NSLocalizedString(@"loc_sta_desc", NULL) ]
    } mutableCopy ];
    
    _mapAnnotations = [[NSMutableArray alloc] init];
    [ self addAnnotations:_mapLocations buffer:_mapAnnotations ];
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    switch ( status ) {
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways:
            _userLocationBtn.alpha = 1;
            [ _userLocationBtn setEnabled:YES ];
            
            _locMan.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            _locMan.distanceFilter = 200;
            [ _locMan startUpdatingLocation ];
            
            _theMap.showsUserLocation = YES;
            
            [ self reloadUserLocation:_mapLocations[@"User"] ];
            [ self showLocation:_mapLocations[@"User"] withZoom:0.05 ];
            break;
            
        case kCLAuthorizationStatusNotDetermined:
            [ _locMan requestWhenInUseAuthorization ];
            break;
            
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            [ self displayAlert:NSLocalizedString(@"no_auth_title", NULL) withMessage:NSLocalizedString(@"no_auth_message", NULL) buttonText:NSLocalizedString(@"no_auth_btn", NULL) ];
            _userLocationBtn.alpha = 0.4;
            [ _userLocationBtn setEnabled:NO ];
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) showLocation:(MapLocation *)location withZoom:(float)zoom {
    
    MKCoordinateRegion region = { {0, 0}, {0, 0} };
    
    region.center.latitude =  location.latitude;
    region.center.longitude = location.longitude;
    
    region.span.latitudeDelta = region.span.longitudeDelta = zoom;
    
    [ _theMap setRegion:region animated:YES ];
    
}

- (void) addAnnotations:(NSMutableDictionary *)locations buffer:(NSMutableArray *)buffer {
    
    for ( id key in locations ) {
        
        MKPointAnnotation *point = [ [MKPointAnnotation alloc] init ];
        MapLocation *location = [ locations objectForKey:key ];
        
        if ( location.name != NULL ) {
            point.coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude);
            point.title     = location.name;
            point.subtitle  = location.desc;
            
            [ buffer addObject:point ];
        }
    }

    [ _theMap addAnnotations:buffer ];
    
}

- (void) reloadUserLocation:(MapLocation *)userLocation {
    _locMan = [ [CLLocationManager alloc] init ];
    userLocation.latitude  = _locMan.location.coordinate.latitude;
    userLocation.longitude = _locMan.location.coordinate.longitude;
}

- (void) displayAlert:(NSString *)title withMessage:(NSString *)message buttonText:(NSString *)bttext {
    
    UIAlertController *alert = [ UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert ];
    
    [ alert addAction:[ UIAlertAction actionWithTitle:bttext style:UIAlertActionStyleDefault handler:^( UIAlertAction *action ) {
        [ self dismissViewControllerAnimated:YES completion:nil ];
    }]];
    
    [ self presentViewController:alert animated:YES completion:nil ];
    
}

- (void) changeLayoutColor:(UIColor *)color {
    
    _MapTypeBtns.tintColor = color;
    
    NSArray *imageButtons = @[ _userLocationBtn,
                               _dorBtn,
                               _c13Btn,
                               _stadiumBtn ];
    
    NSArray *buttonsImages = @[ @"UserLocationDefault",
                                     @"DoRDefault",
                                     @"C13Default",
                                     @"StadiumDefault",
                                     @"UserLocationTapped",
                                     @"DoRTapped",
                                     @"C13Tapped",
                                     @"StadiumTapped" ];
    

    if ( color == [UIColor whiteColor] ) {
        [imageButtons enumerateObjectsUsingBlock:^(id button, NSUInteger idx, BOOL *stop) {
            [ button setImage:[UIImage imageNamed:[ NSString stringWithFormat:@"%@%@",[buttonsImages objectAtIndex:idx], @"White" ] ] forState:UIControlStateNormal ];
            [ button setImage:[UIImage imageNamed:[ NSString stringWithFormat:@"%@%@",[buttonsImages objectAtIndex:(idx +4)], @"White" ] ] forState:UIControlStateHighlighted ];
        }];
    } else {
        [imageButtons enumerateObjectsUsingBlock:^(id button, NSUInteger idx, BOOL *stop) {
            [ button setImage:[UIImage imageNamed:[buttonsImages objectAtIndex:idx]] forState:UIControlStateNormal ];
            [ button setImage:[UIImage imageNamed:[buttonsImages objectAtIndex:(idx+4)]] forState:UIControlStateHighlighted ];
        }];
    }
    
}

@end
