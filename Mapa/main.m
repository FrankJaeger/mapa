//
//  main.m
//  Mapa
//
//  Created by Przemek on 26.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
