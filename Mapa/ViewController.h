//
//  ViewController.h
//  Mapa
//
//  Created by Przemek on 26.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapLocation.h"

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *theMap;
@property (strong) CLLocationManager *locMan;
@property (strong, nonatomic) IBOutlet UISegmentedControl *MapTypeBtns;
@property (strong, nonatomic) IBOutlet UIButton *userLocationBtn;
@property (strong, nonatomic) IBOutlet UIButton *dorBtn;
@property (strong, nonatomic) IBOutlet UIButton *c13Btn;
@property (strong, nonatomic) IBOutlet UIButton *stadiumBtn;

@property NSMutableDictionary *mapLocations;
@property NSMutableArray *mapAnnotations;

- (void) displayAlert:(NSString *)title withMessage:(NSString *)message buttonText:(NSString *)bttext;
- (void) addAnnotations:(NSMutableDictionary *)locations buffer:(NSMutableArray *)buffer;

@end

