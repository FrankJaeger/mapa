//
//  MapLocation.h
//  Mapa
//
//  Created by Przemek on 28.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapLocation : NSObject

@property double latitude;
@property double longitude;
@property NSString *name;
@property NSString *desc;

+ (id) getWithLatitude:(double)latitude andLongitude:(double)longitude name:(NSString *)name andDescription:(NSString *)description;

@end
