//
//  MapLocation.m
//  Mapa
//
//  Created by Przemek on 28.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "MapLocation.h"

@implementation MapLocation

+ (id) getWithLatitude:(double)latitude andLongitude:(double)longitude name:(NSString *)name andDescription:(NSString *)description{
    
    MapLocation *location = [[ [self class] alloc] init ];
    
    location.latitude  = latitude;
    location.longitude = longitude;
    location.name      = name;
    location.desc      = description;
    
    return location;
}

@end
